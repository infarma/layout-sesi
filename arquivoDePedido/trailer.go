package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	NumeroSequencial         int32 `json:"NumeroSequencial"`
	TipoRegistro             int32 `json:"TipoRegistro"`
	QuantidadeRegistroTipo02 int64 `json:"QuantidadeRegistroTipo02"`
	Filler                   int32 `json:"Filler"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeRegistroTipo02, "QuantidadeRegistroTipo02")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":         {0, 7, 0},
	"TipoRegistro":             {7, 9, 0},
	"QuantidadeRegistroTipo02": {9, 21, 0},
	"Filler":                   {21, 25, 0},
}
