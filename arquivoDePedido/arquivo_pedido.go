package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Header  Header  `json:"Header"`
	Item    []Item  `json:"Item"`
	Trailer Trailer `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		var index int32
		if identificador == "01" {
			err = arquivo.Header.ComposeStruct(string(runes))
		} else if identificador == "02" {
			err = arquivo.Item[index].ComposeStruct(string(runes))
			index++
		} else if identificador == "03" {
			err = arquivo.Trailer.ComposeStruct(string(runes))
		}
	}
	return arquivo, err
}
