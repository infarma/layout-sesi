## Arquivo de Pedido
gerador-layouts arquivoDePedido header NumeroSequencial:int32:0:7 TipoRegistro:int32:7:9 CodigoCliente:string:9:23 DataGeracao:int32:23:31 HoraGeracao:int32:31:35 NumeroPedido:int64:35:49 PedidoConsulta:int32:49:50 FormaPagammento:string:50:59 CodigoInternoFilial:int32:59:64

gerador-layouts arquivoDePedido item NumeroSequencial:int32:0:7 TipoRegistro:int32:7:9 CodigoProduto:int64:9:22 Fixo:int64:22:35 Quantidade:int32:35:40 Filler:int32:40:44

gerador-layouts arquivoDePedido trailer NumeroSequencial:int32:0:7 TipoRegistro:int32:7:9 QuantidadeRegistroTipo02:int64:9:21 Filler:int32:21:25


## Retorno de Pedido
gerador-layouts retornoDePedido header NumeroSequencial:int32:0:7 TipoRegistro:int32:7:9 CodigoCliente:string:9:23 DataGeracao:int32:23:31 HoraGeracao:int32:31:35 NumeroPedido:int64:35:49 PedidoConsulta:int32:49:50 NumeroPedidoFornecedor:int64:50:64

gerador-layouts retornoDePedido item NumeroSequencial:int32:0:7 TipoRegistro:int32:7:9 CodigoProduto:int64:9:22 ValorUnitario:int64:22:35 Quantidade:int32:35:39 PercentualDesconto:int64:39:48 CodigoMensagemRetorno:int32:48:54

gerador-layouts retornoDePedido trailer NumeroSequencial:int32:0:2 TipoRegistro:int32:2:9 Fixo:int64:9:19 ValorBruto:float64:19:29:2 ValorDesconto:float64:29:39:2 Filler:int32:39:43