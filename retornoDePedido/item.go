package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Item struct {
	NumeroSequencial      int32 `json:"NumeroSequencial"`
	TipoRegistro          int32 `json:"TipoRegistro"`
	CodigoProduto         int64 `json:"CodigoProduto"`
	ValorUnitario         int64 `json:"ValorUnitario"`
	Quantidade            int32 `json:"Quantidade"`
	PercentualDesconto    int64 `json:"PercentualDesconto"`
	CodigoMensagemRetorno int32 `json:"CodigoMensagemRetorno"`
}

func (i *Item) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItem

	err = posicaoParaValor.ReturnByType(&i.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorUnitario, "ValorUnitario")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PercentualDesconto, "PercentualDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoMensagemRetorno, "CodigoMensagemRetorno")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItem = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":      {0, 7, 0},
	"TipoRegistro":          {7, 9, 0},
	"CodigoProduto":         {9, 22, 0},
	"ValorUnitario":         {22, 35, 0},
	"Quantidade":            {35, 39, 0},
	"PercentualDesconto":    {39, 48, 0},
	"CodigoMensagemRetorno": {48, 54, 0},
}
