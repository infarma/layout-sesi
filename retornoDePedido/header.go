package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Header struct {
	NumeroSequencial       int32  `json:"NumeroSequencial"`
	TipoRegistro           int32  `json:"TipoRegistro"`
	CodigoCliente          string `json:"CodigoCliente"`
	DataGeracao            int32  `json:"DataGeracao"`
	HoraGeracao            int32  `json:"HoraGeracao"`
	NumeroPedido           int64  `json:"NumeroPedido"`
	PedidoConsulta         int32  `json:"PedidoConsulta"`
	NumeroPedidoFornecedor int64  `json:"NumeroPedidoFornecedor"`
}

func (h *Header) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesHeader

	err = posicaoParaValor.ReturnByType(&h.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.DataGeracao, "DataGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.HoraGeracao, "HoraGeracao")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedido, "NumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.PedidoConsulta, "PedidoConsulta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&h.NumeroPedidoFornecedor, "NumeroPedidoFornecedor")
	if err != nil {
		return err
	}

	return err
}

var PosicoesHeader = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial":       {0, 7, 0},
	"TipoRegistro":           {7, 9, 0},
	"CodigoCliente":          {9, 23, 0},
	"DataGeracao":            {23, 31, 0},
	"HoraGeracao":            {31, 35, 0},
	"NumeroPedido":           {35, 49, 0},
	"PedidoConsulta":         {49, 50, 0},
	"NumeroPedidoFornecedor": {50, 64, 0},
}
