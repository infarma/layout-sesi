package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	NumeroSequencial int32   `json:"NumeroSequencial"`
	TipoRegistro     int32   `json:"TipoRegistro"`
	Fixo             int64   `json:"Fixo"`
	ValorBruto       float64 `json:"ValorBruto"`
	ValorDesconto    float64 `json:"ValorDesconto"`
	Filler           int32   `json:"Filler"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.NumeroSequencial, "NumeroSequencial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Fixo, "Fixo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorBruto, "ValorBruto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.ValorDesconto, "ValorDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Filler, "Filler")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"NumeroSequencial": {0, 2, 0},
	"TipoRegistro":     {2, 9, 0},
	"Fixo":             {9, 19, 0},
	"ValorBruto":       {19, 29, 2},
	"ValorDesconto":    {29, 39, 2},
	"Filler":           {39, 43, 0},
}
